import akshare as ak
import pandas as pd
import os

from config import ml_config


def download_convs_hist(tick: str = "sh113030") -> pd.DataFrame:
    """download all historical convertable data

    Args:
        tick (str, optional): tick number. Defaults to "sh113030".

    Returns:
        pd.DataFrame: contains open, high, low, close and vol data
    """
    save_path = f"{ml_config.SOURCE_DATA_PATH}/{tick}.csv"
    if os.path.exists(save_path):
        df = pd.read_csv(save_path, header=0, index_col="date")
    else:
        df = ak.stock_zh_index_daily_tx(tick)
        df.columns = [c.title() for c in df.columns]  # Enforce colnames as "Open, etc"
        df.rename(columns={"Amount": "Volume"}, inplace=True)
        df.to_csv(save_path)
    return df


def download_convs_meta() -> pd.DataFrame:
    """all conv meta data

    Returns:
        pd.DataFrame: [Convertable tick, exchange, name etc]
    """
    df = ak.bond_zh_cov()
    return df


def download_index_hist(tick: str) -> pd.DataFrame:
    # 上证综指 sh000001
    return ak.stock_zh_index_daily(tick)