import matplotlib.pyplot as plt
class Display:

    @staticmethod
    def plot_prediction(original, trained, train_len, nn, symbol):

        """
        Function to plot all portfolio cumulative returns
        """
        # Set a palette so that all 14 lines can be better differentiated
        color_palette = ['#e6194b', '#3cb44b', '#4363d8']
        fig, ax = plt.subplots(figsize=(14, 6))
        ax.plot(original.index, original, '-', label="Original price", linewidth=2, color=color_palette[0])
        ax.plot(trained.iloc[:train_len].index, trained.iloc[:train_len], '-', label="Trained price", linewidth=2,
                color=color_palette[1], alpha=0.8)
        ax.plot(trained.iloc[train_len:].index, trained.iloc[train_len:], '-', label="Predicted price", linewidth=2,
                color=color_palette[2])
        plt.legend()
        plt.xlabel('Date')
        plt.ylabel('Stock price')
        plt.title('Original, trained & predicted stock price trained on {} model for {}'.format(nn, symbol))
        plt.subplots_adjust(hspace=0.5)

        # Display and save the graph
        plt.savefig('prediction_{}_{}.png'.format(nn, symbol))
        # Inform user graph is saved and the program is ending.
        print(
            "Plot saved as prediction_{}.png. When done viewing, please close this plot for next plot. Thank You!".format(
                nn))
        plt.show()

    @staticmethod
    def features_visualization(stock_context_fs, symbol):
        df_values = stock_context_fs.values
        i = 1
        # plot each column
        plt.figure(figsize=[16, 24])
        for i in range(1, len(stock_context_fs.columns)):
            plt.subplot(len(stock_context_fs.columns), 1, i)
            plt.plot(df_values[:, i], lw=1)
            plt.title(stock_context_fs.columns[i], y=0.5, loc='right')
            i += 1
        plt.savefig('features_visualization_{}.png'.format(symbol))
        plt.show()