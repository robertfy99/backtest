# %%
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf

import sys

sys.path.append("../api")

from domain.services import data_service

mpl.rcParams["figure.figsize"] = (8, 6)
mpl.rcParams["axes.grid"] = False
# %%
df_raw = data_service.read_raw_data()
df = data_service.preprocess_raw(df_raw)
# %%
df_geli = df.query("Tick=='110030'")[["Date", "Close"]].set_index("Date")

# %%
df_geli.plot(subplots=True)
# %%
df_geli.describe().T
# %%
# https://www.tensorflow.org/tutorials/structured_data/time_series
# https://medium.com/@khairulomar/deconstructing-time-series-using-fourier-transform-e52dd535a44e
fft = tf.signal.rfft(df.Close)
f_per_dataset = np.arange(0, len(fft))

n_samples_h = len(df.Close)
hours_per_year = 24 * 365.2524
years_per_dataset = n_samples_h / (hours_per_year)

f_per_year = f_per_dataset / years_per_dataset
plt.step(f_per_year, np.abs(fft))
plt.xscale("log")
plt.ylim(0, 400000)
plt.xlim([0.1, max(plt.xlim())])
plt.xticks([1, 365.2524], labels=["1/Year", "1/day"])
_ = plt.xlabel("Frequency (log scale)")
# %%
