# %%
import pandas as pd
from build_dataset import Data
from services.display_service import Display
from train import Training
from config import ml_config

# Set the print canvas right
pd.set_option("display.float_format", lambda x: "%.2f" % x)
pd.set_option("display.max_columns", 14)
pd.set_option("display.width", 1600)

PREDICTION_AHEAD = ml_config.PREDICTION_AHEAD
TRAIN_PORTION = ml_config.TRAIN_PORTION
SYMBOL = "sh113011"

# %%
stock_data = Data(SYMBOL)
stock_data.preprocessing()
stock_raw, stock_fs = stock_data.X, stock_data.X_fs

market_data = Data("sh000001")
market_data.preprocessing()
market_raw, market_fs = market_data.X, market_data.X_fs

stock_fs = stock_fs.dropna()
market_fs.columns = [c + "_M" for c in market_fs.columns]
market_fs = market_fs.drop(["Predict-Y_M"], axis=1).dropna()
stock_context_fs = pd.concat([market_fs, stock_fs], axis=1)
stock_context_fs = stock_context_fs.dropna()
stock_context_fs


# %%
# Display.features_visualization(stock_context_fs, SYMBOL)

# %%
train_model = Training(stock_data, stock_fs, stock_context_fs, TRAIN_PORTION)
train_model.modelling("LSTM", SYMBOL)

# %%
