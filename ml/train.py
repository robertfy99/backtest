import numpy as np
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
import pandas as pd

from models.lstm import Models
from services.display_service import Display

es = EarlyStopping(monitor="val_loss", mode="min", verbose=1, patience=40)


class Training:
    def __init__(self, stock_data, stock_fs, stock_context_fs, train_portion):

        self.stock_data = stock_data
        self.stock_fs = stock_fs
        self.df_values = stock_context_fs.values
        self.stock_context_fs = stock_context_fs
        self.train_portion = train_portion

    def plot_training(self, history, nn, symbol):
        # plot history
        plt.plot(history.history["loss"], label="train")
        plt.plot(history.history["val_loss"], label="test")
        plt.legend()
        plt.title("Training loss history for {} model for {}".format(nn, symbol))
        plt.savefig("history_{}_{}.png".format(nn, symbol))
        plt.show()

    def train_model(self, model, train_X, train_y, model_type, symbol):

        pre_trained = False
        if model_type == "LSTM":
            batch_size = 4
            mc = ModelCheckpoint(
                "best_lstm_model_{}.h5".format(symbol),
                monitor="val_loss",
                save_weights_only=False,
                mode="min",
                verbose=1,
                save_best_only=True,
            )
            try:
                model = load_model("./best_lstm_model_{}.h5".format(symbol))
                print("Loading pre-saved model ...")
                pre_trained = True
            except:
                print("No pre-saved model, training new model.")
                pass
        elif model_type == "CNN":
            batch_size = 8
            mc = ModelCheckpoint(
                "best_cnn_model_{}.h5".format(symbol),
                monitor="val_loss",
                save_weights_only=False,
                mode="min",
                verbose=1,
                save_best_only=True,
            )
            try:
                model = load_model("./best_cnn_model_{}.h5".format(symbol))
                print("Loading pre-saved model ...")
                pre_trained = True
            except:
                print("No pre-saved model, training new model.")
                pass
        # fit network

        if not pre_trained:
            print("\n")
            print("{} model training starts now for {} ...".format(model_type, symbol))
            print("\n")
            history = model.fit(
                train_X,
                train_y,
                epochs=500,
                batch_size=batch_size,
                validation_split=0.2,
                verbose=2,
                shuffle=False,
                # callbacks=[es, mc, tb, LearningRateTracker()])
                callbacks=[es, mc],
            )

            if model_type == "LSTM":
                model.save("./best_lstm_model_{}.h5".format(symbol))
            elif model_type == "CNN":
                model.save("./best_cnn_model_{}.h5".format(symbol))

        elif pre_trained:
            history = []

        return history, model, pre_trained

    def split_data(self, nn):
        # split into train and test sets
        n_train = self.train_portion * self.df_values.shape[0]
        train = self.df_values[: int(n_train), :]
        test = self.df_values[int(n_train) :, :]
        # split into input and outputs
        train_X, train_y = train[:, :-1], train[:, -1]
        test_X, test_y = test[:, :-1], test[:, -1]
        # reshape input to be 3D [samples, timesteps, features]
        if nn == "CNN":
            train_X = train_X.reshape((train_X.shape[0], train_X.shape[1], 1))
            test_X = test_X.reshape((test_X.shape[0], test_X.shape[1], 1))
        elif nn == "LSTM":
            train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
            test_X = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))

        print("\n")
        print("Train feature data shape:", train_X.shape)
        print("Train label data shape:", train_y.shape)
        print("Test feature data shape:", test_X.shape)
        print("Test label data shape:", test_y.shape)

        return train_X, train_y, test_X, test_y

    def scoring(self, model, train_X, train_y, test_X, test_y, nn):

        print("Training and test scores for {} model".format(nn))
        trainScore = model.evaluate(train_X, train_y, verbose=0)
        for i, m in enumerate(model.metrics_names):
            print("Train {0}: {1:5.4f}".format(m, trainScore[i]))

        testScore = model.evaluate(test_X, test_y, verbose=0)
        for i, m in enumerate(model.metrics_names):
            print("Test {0}: {1:5.4f}".format(m, testScore[i]))
        print("\n")

    def get_prediction(self, model, train_X, test_X, nn, symbol):

        # Get the predicted price
        predicted_y = model.predict(test_X, batch_size=None, verbose=0, steps=None)
        # Get the trained price
        trained_y = model.predict(train_X, batch_size=None, verbose=0, steps=None)
        # Vertically stack trained and predicted price into a dataframe to form a vector of price produced by CNN
        y = pd.DataFrame(
            data=np.vstack((trained_y, predicted_y)),
            columns=[nn],
            index=self.stock_context_fs.index,
        )
        # Assemble a dataframe with normalized price of original and CNN trained/predicted price
        y_df = pd.concat([self.stock_context_fs[["Predict-Y"]], y], axis=1)
        # Assemble the dataframe resembles of the original stock dataframe for inverse transformation.
        df = self.stock_fs.loc[y_df.index]
        # Replace the label column with the CNN trained & predicted price column
        df[["Predict-Y"]] = y_df[[nn]]
        # Get it inverse transformed back to normal price
        recovered_data = self.stock_data.minmaxed_scaler.inverse_transform(df)
        recovered_data = pd.DataFrame(
            data=recovered_data, columns=self.stock_fs.columns, index=df.index
        )

        Display.plot_prediction(
            self.stock_data.original[["Predict-Y"]].loc[recovered_data.index],
            recovered_data[["Predict-Y"]],
            len(trained_y),
            nn,
            symbol,
        )

    def modelling(self, nn, symbol):

        train_X, train_y, test_X, test_y = self.split_data(nn)

        if nn == "CNN":
            model = Models.build_cnn_model(train_X)
        elif nn == "LSTM":
            model = Models.build_rnn_model(train_X)
        print("\n")
        history, model, pre_trained = self.train_model(
            model, train_X, train_y, nn, symbol
        )
        print("\n")
        if not pre_trained:
            self.plot_training(history, nn, symbol)
        self.scoring(model, train_X, train_y, test_X, test_y, nn)
        self.get_prediction(model, train_X, test_X, nn, symbol)