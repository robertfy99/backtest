from pydantic import BaseSettings, validator


class Config(BaseSettings):
    """
    docstring
    """

    IS_LOCAL: bool = True
    GCS_BUCKET: str = "backtest-304020.appspot.com"
    GCS_FILE_PATH: str = "source.csv"
    SOURCE_DATA_PATH: str = "../raw_data/"
    READ_NROWS = 100
    COMMISSION: float = 0.0002
    CASH: float = 10000
    PREDICTION_AHEAD = 1
    TRAIN_PORTION = 0.85

    @validator("SOURCE_DATA_PATH", pre=True, always=True)
    def set_gcs_file_path(cls, v, values, **kwargs):
        if values["IS_LOCAL"]:
            return v
        else:
            return f"gcs://{values['GCS_BUCKET']}/{values['GCS_FILE_PATH']}"

    class Config:
        case_sensitive = True
        env_prefix = "BACKTEST_"


ml_config = Config()
