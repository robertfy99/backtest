# %%
from services.data_service import download_convs_hist, download_convs_meta

# %%
df_names = download_convs_meta()
df_names.to_csv("./data/conv_meta.csv", encoding="gb2312", index=False)

# %%
def download_all():
    tradable = df_names.query("债现价!='-'")
    error = []
    for ind, tick in enumerate(tradable.债券代码.unique()):
        print(f"{ind}/{len(tradable)}")
        try:
            download_convs_hist(f"sh{tick}").to_csv(f"./data/{tick}_hist.csv")
        except Exception:
            error.append(tick)
    return error


# %%
download_all()
# %%
