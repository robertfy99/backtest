import tensorflow as tf
import tensorflow.keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation
from tensorflow.keras.layers import LSTM
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Reshape
from tensorflow.keras.layers import (
    Conv1D,
    MaxPooling1D,
    LeakyReLU,
    PReLU,
    GlobalAveragePooling1D,
)
from tensorflow.keras import regularizers
from tensorflow.keras import backend as K


class Models:
    @staticmethod
    def build_cnn_model(train_X):
        print("\n")
        print("CNN + RNN LSTM model architecture ")
        model = Sequential()
        model.add(
            Conv1D(
                activation="linear",
                kernel_initializer="uniform",
                bias_initializer="zeros",
                input_shape=(train_X.shape[1], train_X.shape[2]),
                # kernel_regularizer=regularizers.l2(0.0001),
                # activity_regularizer=regularizers.l1(0.0001),
                filters=256,
                kernel_size=8,
            )
        )
        model.add(
            Conv1D(
                activation="linear",
                kernel_initializer="uniform",
                bias_initializer="zeros",
                filters=256,
                kernel_size=6,
            )
        )
        model.add(Dropout(0.25))
        model.add(MaxPooling1D(3))
        model.add(LSTM(128, kernel_initializer="uniform", bias_initializer="zeros"))
        model.add(Dropout(0.25))
        model.add(Dense(1))
        # optimizer = tensorflow.keras.optimizers.Adam(lr=0.00005, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
        # optimizer = tensorflow.keras.optimizers.Adam(lr=0.00001)
        optimizer = tensorflow.keras.optimizers.Adagrad(
            lr=0.01, epsilon=1e-08, decay=0.0002
        )
        # optimizer = tensorflow.keras.optimizers.RMSprop(lr=0.01, rho=0.9, epsilon=1e-08, decay=0.0002)
        model.compile(loss="mae", optimizer=optimizer, metrics=["mse", "mae"])
        model.summary()

        return model

    @staticmethod
    def build_rnn_model(train_X):
        # design network
        print("\n")
        print("RNN LSTM model architecture >")
        model = Sequential()
        model.add(
            LSTM(
                256,
                kernel_initializer="random_uniform",
                bias_initializer="zeros",
                return_sequences=True,
                stateful=False,
                input_shape=(train_X.shape[1], train_X.shape[2]),
            )
        )
        model.add(Dropout(0.25))
        model.add(
            LSTM(
                64,
                kernel_initializer="random_uniform",
                # kernel_regularizer=regularizers.l2(0.001),
                # activity_regularizer=regularizers.l1(0.001),
                bias_initializer="zeros",
            )
        )
        model.add(Dropout(0.25))
        model.add(Dense(1))
        optimizer = tensorflow.keras.optimizers.RMSprop(
            lr=0.01, rho=0.9, epsilon=1e-08, decay=0.0002
        )
        # optimizer = tensorflow.keras.optimizers.Adagrad(lr=0.03, epsilon=1e-08, decay=0.00002)
        # optimizer = tensorflow.keras.optimizers.Adam(lr=0.0001)
        # optimizer = tensorflow.keras.optimizers.Nadam(lr=0.0002, beta_1=0.9, beta_2=0.999, schedule_decay=0.004)
        # optimizer = tensorflow.keras.optimizers.Adamax(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)
        # optimizer = tensorflow.keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)

        model.compile(loss="mae", optimizer=optimizer, metrics=["mse", "mae"])
        model.summary()
        return model