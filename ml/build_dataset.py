# %%
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt

from services.data_service import download_convs_hist
from services.feature_service import get_technical_indicators, FeatureSelector

# %%
PREDICTION_AHEAD = 1
TICKER = 'sh110030'

# %%


class Data:
    def __init__(self, tick) -> None:
        self.tick = tick
        self._get_hist_data()
        self._get_technical_indicators()

    def _get_hist_data(self):
        self.tick_df = download_convs_hist(self.tick)

    def _get_technical_indicators(self):
        self.ta = get_technical_indicators(self.tick_df)

    def label(self, df, seq_length):
        return (df["Returns"] > 0).astype(int)

    def preprocessing(self):

        self.tick_df["Returns"] = pd.Series(
            (self.tick_df["Close"] / self.tick_df["Close"].shift(1) - 1) * 100,
            index=self.tick_df.index,
        )
        seq_length = 3
        self.X = self.tick_df[["Open", "Close", "High", "Low", "Volume"]]
        self.y = self.label(self.tick_df, seq_length)
        X_shift = [self.X]
        for i in range(1, seq_length):
            shifted_df = self.tick_df[["Open", "Close", "High", "Low", "Volume"]].shift(
                i
            )
            X_shift.append(shifted_df / shifted_df.mean())
        ohlc = pd.concat(X_shift, axis=1)
        ohlc.columns = sum(
            [
                [
                    c + "T-{}".format(i)
                    for c in ["Open", "Close", "High", "Low", "Volume"]
                ]
                for i in range(seq_length)
            ],
            [],
        )
        self.ta.index = ohlc.index
        self.X = pd.concat([ohlc, self.ta], axis=1)
        self.Xy = pd.concat([self.X, self.y], axis=1)

        fs = FeatureSelector(data=self.X, labels=self.y)
        fs.identify_all(
            selection_params={
                "missing_threshold": 0.6,
                "correlation_threshold": 0.9,
                "task": "regression",
                "eval_metric": "auc",
                "cumulative_importance": 0.99,
            }
        )
        self.X_fs = fs.remove(methods="all", keep_one_hot=True)
        # Add the y label, close price of days ahead(PREDICTION_AHEAD)
        shifted_close_price = self.tick_df["Close"].shift(-PREDICTION_AHEAD)
        self.X_fs["Predict-Y"] = shifted_close_price.loc[self.X_fs.index].dropna()
        self.original = self.X_fs
        self.scale_data()
        self.Xy_fs = pd.concat([self.X_fs, self.y], axis=1)

    def scale_data(self):
        """
        This function scale raw data for effective ML training. Stadardization, normalization and MInMax methods are performed.
        """
        # Get the feature and target labels
        self.X_fs = self.X_fs.dropna()

        # Normalization scaling
        self.normalized_scaler = Normalizer()
        normalized = self.normalized_scaler.fit_transform(self.X_fs)
        self._normalized = pd.DataFrame(
            normalized, index=self.X_fs.index, columns=self.X_fs.columns
        )

        # Standardization scaling
        self.standardized_scaler = StandardScaler()
        standardized = self.standardized_scaler.fit_transform(self.X_fs)
        self._standardized = pd.DataFrame(
            standardized, index=self.X_fs.index, columns=self.X_fs.columns
        )

        # MinMax scaling
        self.minmaxed_scaler = MinMaxScaler(feature_range=(0, 1))
        minmaxed = self.minmaxed_scaler.fit_transform(self.X_fs)
        self._minmaxed = pd.DataFrame(
            minmaxed, index=self.X_fs.index, columns=self.X_fs.columns
        )

        self.X_fs = self._minmaxed
        # self.X_fs = self._normalized
        # self.X_fs = self._standardized
        self.y = self.y.loc[self.X_fs.index]
# %%

# stock_data = Data("sh110030")
# stock_data.preprocessing()
# stock_raw, stock_fs = stock_data.X, stock_data.X_fs


# # %%
# market_data = Data('sh000001')
# market_data.preprocessing()
# market_raw, market_fs = market_data.X, market_data.X_fs

# %%
# stock_fs = stock_fs.dropna()
# market_fs.columns = [c + '_M' for c in market_fs.columns]
# market_fs = market_fs.drop(['Predict-Y_M'], axis=1).dropna()
# stock_context_fs = pd.concat([market_fs, stock_fs], axis=1)
# stock_context_fs = stock_context_fs.dropna()
# stock_context_fs


# %%
# df_values = stock_context_fs.values
# i = 1
# # plot each column
# plt.figure(figsize=[16, 24])
# for i in range(1, len(stock_context_fs.columns)):
#     plt.subplot(len(stock_context_fs.columns), 1, i)
#     plt.plot(df_values[:, i], lw=1)
#     plt.title(stock_context_fs.columns[i], y=0.5, loc='right')
#     i += 1
# plt.savefig('features_visualization_{}.png'.format(TICKER))
# plt.show()
# %%
