import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { BacktestResponse } from '../interface';

const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
});



function Row(props: BacktestResponse): JSX.Element {
    const historyData = props.TradesHistory;
    const { TradesHistory, ...summaryData } = props;
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();

    return (
        <React.Fragment>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {summaryData.Name}
                </TableCell>
                <TableCell align="right">{summaryData.Start}</TableCell>
                <TableCell align="right">{summaryData.End}</TableCell>
                <TableCell align="right">{summaryData.Duration}</TableCell>
                <TableCell align="right">{summaryData.ExposureTime_Pct}</TableCell>
                <TableCell align="right">{summaryData.EquityFinal}</TableCell>
                <TableCell align="right">{summaryData.EquityPeak}</TableCell>
                <TableCell align="right">{summaryData.Return_Pct}</TableCell>
                <TableCell align="right">{summaryData.BuyAndHoldReturn_Pct}</TableCell>
                <TableCell align="right">{summaryData.ReturnAnnualised_Pct}</TableCell>
                <TableCell align="right">{summaryData.VolatilityAnnualised_Pct}</TableCell>
                <TableCell align="right">{summaryData.Sharpe_Ratio}</TableCell>
                <TableCell align="right">{summaryData.Sortino_Ratio}</TableCell>
                <TableCell align="right">{summaryData.Calmar_Ratio}</TableCell>
                <TableCell align="right">{summaryData.MaxDrawDown_Pct}</TableCell>
                <TableCell align="right">{summaryData.AvgDrawDown_Pct}</TableCell>
                <TableCell align="right">{summaryData.MaxDrawDownDuration}</TableCell>
                <TableCell align="right">{summaryData.AvgDrawDownDuration}</TableCell>
                <TableCell align="right">{summaryData.Trades}</TableCell>
                <TableCell align="right">{summaryData.WinRate_Pct}</TableCell>
                <TableCell align="right">{summaryData.BestTrade_Pct}</TableCell>
                <TableCell align="right">{summaryData.WorstTrade_Pct}</TableCell>
                <TableCell align="right">{summaryData.AvgTrade_Pct}</TableCell>
                <TableCell align="right">{summaryData.MaxTradeDuration}</TableCell>
                <TableCell align="right">{summaryData.AvgTradeDuration}</TableCell>
                <TableCell align="right">{summaryData.ProfitFactor}</TableCell>
                <TableCell align="right">{summaryData.Expectancy_Pct}</TableCell>
                <TableCell align="right">{summaryData.SQN}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                History
              </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>EntryTime</TableCell>
                                        <TableCell>EntryPrice</TableCell>
                                        <TableCell align="right">ExitTime</TableCell>
                                        <TableCell align="right">ExitPrice</TableCell>
                                        <TableCell align="right">Duration</TableCell>
                                        <TableCell align="right">EntryBar</TableCell>
                                        <TableCell align="right">ExitBar</TableCell>
                                        <TableCell align="right">PnL</TableCell>
                                        <TableCell align="right">ReturnPct</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {historyData.map((historyRow) => (
                                        <TableRow key={historyRow.EntryTime}>
                                            <TableCell component="th" scope="row">
                                                {historyRow.EntryTime}
                                            </TableCell>
                                            <TableCell>{historyRow.EntryPrice}</TableCell>
                                            <TableCell align="right">{historyRow.ExitTime}</TableCell>
                                            <TableCell align="right">{historyRow.ExitPrice}</TableCell>
                                            <TableCell align="right">{historyRow.Duration}</TableCell>
                                            <TableCell align="right">{historyRow.EntryBar}</TableCell>
                                            <TableCell align="right">{historyRow.ExitBar}</TableCell>
                                            <TableCell align="right">{historyRow.PnL}</TableCell>
                                            <TableCell align="right">{historyRow.ReturnPct}</TableCell>
                                            {/* <TableCell align="right">
                                                {Math.round(historyRow.amount * row.price * 100) / 100}
                                            </TableCell> */}
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

export const SummaryTable: React.FC<BacktestResponse> = (summaryProps) => {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Name</TableCell>
                        <TableCell align="right">Start</TableCell>
                        <TableCell align="right">End</TableCell>
                        <TableCell align="right">Duration</TableCell>
                        <TableCell align="right">ExposureTime_Pct</TableCell>
                        <TableCell align="right">EquityFinal</TableCell>
                        <TableCell align="right">EquityPeak</TableCell>
                        <TableCell align="right">Return_Pct</TableCell>
                        <TableCell align="right">BuyAndHoldReturn_Pct</TableCell>
                        <TableCell align="right">ReturnAnnualised_Pct</TableCell>
                        <TableCell align="right">VolatilityAnnualised_Pct</TableCell>
                        <TableCell align="right">Sharpe_Ratio</TableCell>
                        <TableCell align="right">Sortino_Ratio</TableCell>
                        <TableCell align="right">Calmar_Ratio</TableCell>
                        <TableCell align="right">MaxDrawDown_Pct</TableCell>
                        <TableCell align="right">AvgDrawDown_Pct</TableCell>
                        <TableCell align="right">MaxDrawDownDuration</TableCell>
                        <TableCell align="right">AvgDrawDownDuration</TableCell>
                        <TableCell align="right">Trades</TableCell>
                        <TableCell align="right">WinRate_Pct</TableCell>
                        <TableCell align="right">BestTrade_Pct</TableCell>
                        <TableCell align="right">WorstTrade_Pct</TableCell>
                        <TableCell align="right">AvgTrade_Pct</TableCell>
                        <TableCell align="right">MaxTradeDuration</TableCell>
                        <TableCell align="right">AvgTradeDuration</TableCell>
                        <TableCell align="right">ProfitFactor</TableCell>
                        <TableCell align="right">Expectancy_Pct</TableCell>
                        <TableCell align="right">SQN</TableCell>

                    </TableRow>
                </TableHead>
                <TableBody>
                    {Row(summaryProps)}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
// import MaterialTable from "material-table"
// import { BacktestResponse, TradeHistory } from '../interface'

// function renderHistory(props: TradeHistory[]): JSX.Element {
//     return (<MaterialTable
//         title="History"
//         columns={[
//             { title: 'Size', field: 'Size', type: 'numeric' },
//             { title: 'Entry Bar', field: 'EntryBar', type: 'numeric' },
//             { title: 'Exit Bar', field: 'ExitBar', type: 'numeric' },
//             { title: 'Entry Price', field: 'EntryPrice', type: 'numeric' },
//             { title: 'Exit Price', field: 'ExitPrice', type: 'numeric' },
//             { title: 'Profit/Loss', field: 'PnL', type: 'numeric' },
//             { title: 'Return(%)', field: 'ReturnPct', type: 'numeric' },
//             { title: 'Entry Time', field: 'EntryTime', type: 'date' },
//             { title: 'Exit Time', field: 'ExitTime', type: 'date' },
//             { title: 'Duration', field: 'Duration', type: 'numeric' },
//         ]}
//         data={props}
//     ></MaterialTable>)
// }

// export const SummaryTable: React.FC<BacktestResponse> = (summaryProps) => {
//     const historyData = summaryProps.TradesHistory;
//     const { TradesHistory, children, ...summaryData } = summaryProps;
//     return (
//         <MaterialTable
//             title="Back Test Result"
//             columns={[
//                 { title: 'Start', field: 'Start', type: 'date' },
//                 { title: 'End', field: 'End', type: 'date' },
//                 { title: 'Duration', field: 'Duration', type: 'numeric' },
//                 { title: 'ExposureTime_Pct', field: 'ExposureTime_Pct', type: 'numeric' },
//                 { title: 'EquityFinal', field: 'EquityFinal', type: 'numeric' },
//                 { title: 'EquityPeak', field: 'EquityPeak', type: 'numeric' },
//                 { title: 'Return(%)', field: 'Return_Pct', type: 'numeric' },
//                 { title: 'BuyAndHoldReturn (%)', field: 'BuyAndHoldReturn_Pct', type: 'numeric' },
//                 { title: 'ReturnAnnualised (%)', field: 'ReturnAnnualised_Pct', type: 'numeric' },
//                 { title: 'VolatilityAnnualised (%)', field: 'VolatilityAnnualised_Pct', type: 'numeric' },
//                 { title: 'Sharpe_Ratio', field: 'Sharpe_Ratio', type: 'numeric' },
//                 { title: 'Sortino_Ratio', field: 'Sortino_Ratio', type: 'numeric' },
//                 { title: 'Calmar_Ratio', field: 'Calmar_Ratio', type: 'numeric' },
//                 { title: 'MaxDrawDown_Pct', field: 'MaxDrawDown_Pct', type: 'numeric' },
//                 { title: 'AvgDrawDown_Pct', field: 'AvgDrawDown_Pct', type: 'numeric' },
//                 { title: 'MaxDrawDownDuration', field: 'MaxDrawDownDuration', type: 'numeric' },
//                 { title: 'AvgDrawDownDuration', field: 'AvgDrawDownDuration', type: 'numeric' },
//                 { title: 'Trades', field: 'Trades', type: 'numeric' },
//                 { title: 'WinRate_Pct', field: 'WinRate_Pct', type: 'numeric' },
//                 { title: 'BestTrade_Pct', field: 'BestTrade_Pct', type: 'numeric' },
//                 { title: 'WorstTrade_Pct', field: 'WorstTrade_Pct', type: 'numeric' },
//                 { title: 'AvgTrade_Pct', field: 'AvgTrade_Pct', type: 'numeric' },
//                 { title: 'MaxTradeDuration', field: 'MaxTradeDuration', type: 'numeric' },
//                 { title: 'AvgTradeDuration', field: 'AvgTradeDuration', type: 'numeric' },
//                 { title: 'ProfitFactor', field: 'ProfitFactor', type: 'numeric' },
//                 { title: 'Expectancy_Pct', field: 'Expectancy_Pct', type: 'numeric' },
//                 { title: 'SQN', field: 'SQN', type: 'numeric' },
//             ]}
//             data={[summaryData]}
//             options={{
//                 doubleHorizontalScroll: true
//             }}
//             detailPanel={rowData => {
//                 return (renderHistory(historyData))
//             }}
//         />
//     )
// }
