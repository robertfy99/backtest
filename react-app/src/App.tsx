import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './styles.scss';
import Select from '@material-ui/core/Select';
import { Button, InputLabel, MenuItem } from '@material-ui/core';
import axios from 'axios';
import { Tick, BacktestResponse, TradeHistory } from './interface'
import { SummaryTable } from './components/SummaryTable';

function App() {
  // API URL
  console.log(process.env);
  const API_URI = process.env.REACT_APP_API_URI
  // get all tick options
  const [allTicks, setAllTicks] = useState<Tick[] | undefined>(undefined);
  useEffect(() => {
    axios.get<Tick[]>(API_URI + '/get-ticks').then(({ data }) => setAllTicks(data));
  }, []);

  const [tick, setTick] = useState('格力转债');
  const [strategy, setStrategy] = useState('SmartCross')
  const [summary, setSummary] = useState<BacktestResponse | undefined>(undefined)

  const handleTick = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTick(event.target.value as string);
  };
  const handleStrategy = (event: React.ChangeEvent<{ value: unknown }>) => {
    setStrategy(event.target.value as string);
  }


  const handleSubmit = (event: React.MouseEvent) => {
    const tick_code = allTicks?.find(o => o.Name === tick)?.Tick;
    axios.get<BacktestResponse>(API_URI + '/backtest/' + tick_code).then(({ data }) => setSummary(data));

  }

  return (
    <div className="grid-container">
      <div className='content header'>Back Testing</div>
      <div className='content sidebar'>
        <InputLabel>Source Data</InputLabel>
        <Select
          value={'Internal'}
          onChange={() => { }}
        >
          <MenuItem value={'Internal'}>Internal</MenuItem>
        </Select>
        <InputLabel>Tick</InputLabel>
        <Select
          value={tick}
          onChange={handleTick}
        >
          {allTicks?.map((tick, i) => {
            return <MenuItem key={i} value={tick.Name}>{tick.Name}</MenuItem>
          })}
        </Select>
        <InputLabel>Strategy</InputLabel>
        <Select
          value={strategy}
          onChange={handleStrategy}
        >
          <MenuItem value={'SmartCross'}>SmartCross</MenuItem>
        </Select>
      </div>
      <Button onClick={handleSubmit} variant="contained" color='secondary'>
        Back test
      </Button>
      <div className='content summary'>
        {summary !== undefined ? (<SummaryTable {...summary}></SummaryTable>) : ''}

      </div>
      {/* <div id="bt-plot" className='content chart' /> */}

    </div>
  );
}

export default App;
