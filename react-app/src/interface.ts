import { components } from "./generated-schema/auto-schema"

export type Tick = components['schemas']['Tick']
export type BacktestResponse = components['schemas']['BacktestResponse']
export type TradeHistory= components['schemas']['TradeHistory']