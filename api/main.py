from starlette.responses import JSONResponse
from strategy.SmaCross import SmaCross
from backtesting.test import GOOG
from backtesting import Backtest
from routers import data_router, task_router

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware


app = FastAPI(
    title="Back test strategy API",
    description="For Rachel's back-testing",
    openapi_url="/openapi.json",
    docs_url="/doc",
)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
@app.get("/health")
@app.get("/health/live")
@app.get("/health/ready")
async def health():
    return {"status": "healthy"}


app.include_router(data_router.router, prefix="/data")
app.include_router(task_router.router, prefix="/task")


@app.get("/test")
def test_goog():
    bt = Backtest(GOOG, SmaCross, cash=10000, commission=0.002)
    result = bt.run()[:-1].to_json()
    return JSONResponse(content=result)
