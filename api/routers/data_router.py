from core.config import Config
from typing import List
import os
import pandas as pd
from fastapi import APIRouter, Depends
from domain.dependencies.data_deps import get_bq_client, get_config
from domain.services import data_service, gcp_service

from domain.schema.backtest_schema import TickerHistory


router = APIRouter()


# @router.get("/get-tickers", response_model=List[Ticker])
# async def get_ticks(df_source: DataFrame = Depends(preprocess_raw)):
#     ticks_df = df_source[["Tick", "Exchange", "Name"]].drop_duplicates()
#     ticks = [Ticker(**t[1]) for t in ticks_df.iterrows()]
#     return ticks


@router.get("/get-ticker-hist/{ticker}", response_model=List[TickerHistory])
async def get_ticker_hist(
    ticker, config: Config = Depends(get_config), bq_client=Depends(get_bq_client)
):
    # read from local
    if config.IS_LOCAL:
        local_path = config.SOURCE_DATA_FULL_PATH
        if not os.path.exists(local_path):
            df = pd.read_csv(local_path, encoding="GB2312", header=0, index_col="date")
        else:
            df = data_service.download_convs_hist(
                ticker=ticker, save_local_path=local_path
            )
    # read from bq
    else:
        if bq_client:
            df = gcp_service.read_bq(
                bq_client,
                config.GCP_PROJECT,
                config.BQ_DATASET,
                ticker,
            )
        else:
            raise Exception("No GCP client found, fail to connect")
    return [TickerHistory(**h) for h in df.to_dict(orient="records")]


# @app.route("/upload")
# def upload():
#     # TODO: consider a database, ie.sqlite
#     data = pd.read_excel(
#         "Convertible bond pool_modified.xlsx",
#         header=0,
#         encoding="gb2312",
#         sheet_name=0,
#         index_col=[0, 1],
#         parse_dates=True,
#     )
#     data.to_csv("data.csv", index=None)

#     return jsonify("success")