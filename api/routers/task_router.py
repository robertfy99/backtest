from fastapi_mail.schemas import MessageSchema
from starlette.responses import JSONResponse
from core.config import Config
from domain.schema.task_schema import EmailSchema
from domain.dependencies.data_deps import (
    get_config,
    get_bq_client,
    get_sm_client,
    setup_email,
)
from domain.services import data_service, gcp_service
import pandas as pd
from fastapi import APIRouter, Depends, BackgroundTasks
from fastapi_mail import FastMail

router = APIRouter()


@router.get("/inject-to-bq/{ticker}")
async def get_data(
    ticker: str,
    background_tasks: BackgroundTasks,
    bq_client=Depends(get_bq_client),
    config=Depends(get_config),
    email: FastMail = Depends(setup_email),
) -> JSONResponse:
    # download source and upload
    df = data_service.download_convs_hist(ticker=ticker)
    background_tasks.add_task(
        gcp_service.upload_df_to_bq,
        df,
        bq_client,
        config.GCP_PROJECT,
        config.BQ_DATASET,
        ticker,
    )
    # send email
    message = MessageSchema(
        subject=f"Data Ingestion for {ticker}",
        recipients=["robertf99@gmail.com"],
        body=f"Data Ingestion task for {ticker} in process",
    )
    background_tasks.add_task(email.send_message, message)
    return JSONResponse(status_code=200, content={"message": "email has been sent"})
