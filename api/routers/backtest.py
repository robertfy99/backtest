from numpy import mean
import pandas as pd
from fastapi import APIRouter, Request, Depends

from domain.schema import BacktestResponse, BackTestAllResponse
from domain.services import backtest_service
from domain.dependencies.data_deps import preprocess_raw
from strategy.SmaCross import SmaCross
from core.config import app_config


router = APIRouter()


@router.get("/api/backtest/{tick}", response_model=BacktestResponse)
async def backtest(tick: str, df_source: pd.DataFrame = Depends(preprocess_raw)):
    return await backtest_service.backtest_one(df_source, tick, SmaCross)


@router.get("/api/backtest-all")
async def backtest_all(df_source: pd.DataFrame = Depends(preprocess_raw)):
    backtest_res = await backtest_service.backtest_all(df_source, SmaCross)

    res = BackTestAllResponse(
        EquityFinal=sum([r.EquityFinal for r in backtest_res]),
        NumTicksAll=len(backtest_res),
        AverageEquityFinal=(mean([r.EquityFinal for r in backtest_res])),
        NumTicksWin=sum([1 for r in backtest_res if r.EquityFinal > app_config.CASH]),
        NumTickLoss=sum([1 for r in backtest_res if r.EquityFinal < app_config.CASH]),
        AverageWin=mean(
            [r.EquityFinal for r in backtest_res if r.EquityFinal > app_config.CASH]
        )
        - app_config.CASH,
        AverageLoss=mean(
            [r.EquityFinal for r in backtest_res if r.EquityFinal < app_config.CASH]
        )
        - app_config.CASH,
        AverageDuration=mean([r.Duration for r in backtest_res]),
    )

    return res
