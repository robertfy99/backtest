import logging

logging.basicConfig()
logger = logging.getLogger("backtest")
logger.setLevel(logging.INFO)
