from pydantic import BaseSettings, validator
from pydantic.types import SecretStr


class Config(BaseSettings):
    """
    docstring
    """

    IS_LOCAL: bool = True
    GCP_PROJECT: str = "backtest-304020"
    GCS_BUCKET: str = "backtest-304020.appspot.com"
    GCS_FILE_PATH: str = "source.csv"
    BQ_DATASET: str = "conv_hist"
    BQ_TABLE: str = "sh103011"
    SOURCE_DATA_FULL_PATH: str = "../api/raw_data/source.csv"
    READ_NROWS = 100
    COMMISSION: float = 0.0002
    CASH: float = 10000
    EMAIL_PASS_SECRET_ID: str = "email_password"
    EMAIL_USERNAME: str
    EMAIL_PASSWORD: str

    @validator("SOURCE_DATA_FULL_PATH", pre=True, always=True)
    def set_gcs_file_path(cls, v, values, **kwargs):
        if values["IS_LOCAL"]:
            return v
        else:
            return (
                f"{values['GCP_PROJECT']}:{values['BQ_DATASET']}.{values['BQ_TABLE']}"
            )

    class Config:
        case_sensitive = True
        secrets_dir = "./secret"
        env_prefix = "BACKTEST_"


app_config = Config()
