from google.cloud import storage
from core.logger import logger


def read_gcs(bucket_name, file_path) -> bytes:
    logger.info(f"Create GCS client for: {bucket_name}")
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(file_path)
    return blob.download_as_bytes()
