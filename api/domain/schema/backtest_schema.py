from typing import List, Optional
from pydantic import BaseModel
from datetime import datetime, timedelta


class TickerHistory(BaseModel):
    date: datetime
    open: float
    close: float
    high: float
    low: float
    volume: float


class TradeHistory(BaseModel):
    Size: int
    EntryBar: float
    ExitBar: float
    EntryPrice: float
    ExitPrice: float
    PnL: float
    ReturnPct: float
    EntryTime: datetime
    ExitTime: datetime
    Duration: timedelta


class Ticker(BaseModel):
    Tick: str
    Exchange: str
    Name: str


class BacktestResponse(BaseModel):
    Name: str
    Exchange: str
    Start: datetime
    End: datetime
    Duration: timedelta
    ExposureTime_Pct: float
    EquityFinal: float
    EquityPeak: float
    Return_Pct: float
    BuyAndHoldReturn_Pct: float
    ReturnAnnualised_Pct: float
    VolatilityAnnualised_Pct: float
    Sharpe_Ratio: float
    Sortino_Ratio: float
    Calmar_Ratio: float
    MaxDrawDown_Pct: float
    AvgDrawDown_Pct: float
    MaxDrawDownDuration: timedelta
    AvgDrawDownDuration: timedelta
    Trades: int
    WinRate_Pct: float
    BestTrade_Pct: float
    WorstTrade_Pct: float
    AvgTrade_Pct: float
    MaxTradeDuration: timedelta
    AvgTradeDuration: timedelta
    ProfitFactor: float
    Expectancy_Pct: Optional[float]
    SQN: Optional[float]
    TradesHistory: List[TradeHistory]


class BackTestAllResponse(BaseModel):
    EquityFinal: float
    NumTicksAll: int
    AverageEquityFinal: float
    NumTicksWin: int
    NumTickLoss: int
    AverageWin: float
    AverageLoss: float
    AverageDuration: timedelta
