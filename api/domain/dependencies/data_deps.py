from functools import lru_cache
from fastapi import Depends
from fastapi_mail import FastMail
from fastapi_mail.config import ConnectionConfig
from google.cloud import bigquery
from google.cloud import secretmanager
from core.config import Config


from core.config import app_config

# all dependencies are cached
# def preprocess_raw(df: DataFrame = Depends(get_data)) -> DataFrame:
#     df_raw = data_service.read_raw_data()
#     df_source = data_service.preprocess_raw(df_raw)
#     return df_source


def get_bq_client():
    return bigquery.Client(project=app_config.GCP_PROJECT)


def get_sm_client():
    return secretmanager.SecretManagerServiceClient()


def setup_email(sm_client=Depends(get_sm_client)) -> FastMail:
    if app_config.IS_LOCAL:
        pw = app_config.EMAIL_PASSWORD
    else:
        name = f"projects/{app_config.GCP_PROJECT}/secrets/{app_config.EMAIL_PASS_SECRET_ID}/versions/latest"
        response = sm_client.access_secret_version(request={"name": name})
        pw = response.payload.data.decode("UTF-8")

    conf = ConnectionConfig(
        MAIL_USERNAME=app_config.EMAIL_USERNAME,
        MAIL_PASSWORD=pw,
        MAIL_PORT=587,
        MAIL_SERVER="smtp.gmail.com",
        MAIL_FROM_NAME="Google APP Engine (Backtest)",
        MAIL_TLS=True,
        MAIL_SSL=False,
        MAIL_FROM="backtest@app-engine.com"
        # USER_CREDENTIALS = True
    )
    return FastMail(conf)


@lru_cache()
def get_config():
    return Config()