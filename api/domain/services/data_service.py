from pandas import DataFrame
import akshare as ak
from core.logger import logger
import pandas as pd
import os


def download_convs_meta() -> DataFrame:
    """all conv meta data

    Returns:
        pd.DataFrame: [Convertable tick, exchange, name etc]
    """
    df = ak.bond_zh_cov()
    return df


def download_index_hist(tick: str) -> DataFrame:
    # 上证综指 sh000001
    return ak.stock_zh_index_daily(tick)


def download_convs_hist(ticker: str, save_local_path: str = None) -> DataFrame:
    """download ticker historical convertable data

    Args:
        tick (str, optional): tick number. Defaults to "sh113011".

    Returns:
        pd.DataFrame: contains open, high, low, close and vol data
    """

    try:
        df = ak.stock_zh_index_daily_tx(ticker)
        df.columns = [c.lower() for c in df.columns]  # Enforce colnames as "open, etc"
        df.rename(columns={"amount": "volume"}, inplace=True)
    except Exception:
        logger.exception("Download source data failed")
        raise
    if save_local_path:
        df.to_csv(save_local_path, index=True)
    return df
