from google.cloud import bigquery
from pandas import DataFrame
from core.logger import logger


def upload_df_to_bq(df, client, project, dataset, table):
    try:
        table_ref = f"{project}.{dataset}.{table}"
        job_config = bigquery.LoadJobConfig(write_disposition="WRITE_TRUNCATE")
        job = client.load_table_from_dataframe(df, table_ref, job_config=job_config)
        # wait for result
        return job.result()
    except Exception:
        logger.exception(f"Failed to upload dataframe to {dataset}.{table}")
        raise


def read_bq(client, project, dataset, table) -> DataFrame:
    query_sql = "SELECT * from {}.{}.{}".format(project, dataset, table)
    df = client.query(query_sql).to_dataframe()
    return df