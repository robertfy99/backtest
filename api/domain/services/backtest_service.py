from backtesting import Backtest, Strategy
import pandas as pd
import asyncio

from domain.schema import BacktestResponse, TradeHistory
from core.config import app_config
from core.logger import logger


async def backtest_one(
    df_source: pd.DataFrame, tick: str, strategy: Strategy
) -> BacktestResponse:
    logger.info(f"backtesting {tick}...")
    df_tick = df_source.query("Tick==@tick").sort_values("Date")
    # assume one price each day
    backtest_df = pd.DataFrame(
        {
            "Open": df_tick.Close.to_list(),
            "High": df_tick.Close.to_list(),
            "Low": df_tick.Close.to_list(),
            "Close": df_tick.Close.to_list(),
        },
        index=df_tick.Date,
    )
    if backtest_df.shape[0] < 100:
        return None
    try:
        bt = Backtest(
            backtest_df,
            strategy,
            cash=app_config.CASH,
            commission=app_config.COMMISSION,
        )
        # Big assumption for backtest result: NA are 0, for now
        result = bt.run().fillna(0)
        trades = [TradeHistory(**t[1]) for t in result._trades.iterrows()]
        summary = BacktestResponse(
            Name=df_tick.Name.to_list()[0],
            Exchange=df_tick.Exchange.to_list()[0],
            Start=result.Start,
            End=result.End,
            Duration=result.Duration,
            ExposureTime_Pct=result["Exposure Time [%]"],
            EquityFinal=result["Equity Final [$]"],
            EquityPeak=result["Equity Peak [$]"],
            Return_Pct=result["Return [%]"],
            BuyAndHoldReturn_Pct=result["Buy & Hold Return [%]"],
            ReturnAnnualised_Pct=result["Return (Ann.) [%]"],
            VolatilityAnnualised_Pct=result["Volatility (Ann.) [%]"],
            Sharpe_Ratio=result["Sharpe Ratio"],
            Sortino_Ratio=result["Sortino Ratio"],
            Calmar_Ratio=result["Calmar Ratio"],
            MaxDrawDown_Pct=result["Max. Drawdown [%]"],
            AvgDrawDown_Pct=result["Avg. Drawdown [%]"],
            MaxDrawDownDuration=result["Max. Drawdown Duration"],
            AvgDrawDownDuration=result["Avg. Drawdown Duration"],
            Trades=result["# Trades"],
            WinRate_Pct=result["Win Rate [%]"],
            BestTrade_Pct=result["Best Trade [%]"],
            WorstTrade_Pct=result["Worst Trade [%]"],
            AvgTrade_Pct=result["Avg. Trade [%]"],
            MaxTradeDuration=result["Max. Trade Duration"],
            AvgTradeDuration=result["Avg. Trade Duration"],
            ProfitFactor=result["Profit Factor"],
            Expectancy_Pct=result["Expectancy [%]"],
            SQN=result["SQN"],
            TradesHistory=trades,
        )
    except Exception:
        logger.exception(f"{tick} has error")
        raise
    return summary


# async def write_to_
async def backtest_all(df_source: pd.DataFrame, strategy: Strategy):
    tasks = [
        backtest_one(df_source, tick, strategy) for tick in df_source.Tick.unique()
    ]
    res = await asyncio.gather(*tasks)
    return [r for r in res if r is not None]