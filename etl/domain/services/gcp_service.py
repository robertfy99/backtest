from logging import getLogger
from google.cloud import bigquery
from google.cloud.exceptions import NotFound

logger = getLogger()


def upload_df_to_bq(df, projectId, dataset, table):

    bq_client = bigquery.Client(project=projectId)
    try:
        job_config = bigquery.LoadJobConfig(write_disposition="WRITE_TRUNCATE")
        job = bq_client.load_table_from_dataframe(
            df, f"{dataset}.{table}", job_config=job_config
        )
        # wait for result
        return job.result()
    except Exception:
        logger.exception(f"Failed to upload dataframe to {dataset}.{table}")
        raise