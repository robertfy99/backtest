from google.api_core import exceptions
import sys
from logging import getLogger
from google.cloud.bigquery_storage_v1beta2.services.big_query_write import (
    BigQueryWriteAsyncClient,
)
from google.cloud.bigquery_storage_v1beta2.types import (
    WriteStream,
    CreateWriteStreamRequest,
)
from google.cloud import bigquery
from google.cloud.exceptions import NotFound
from google.cloud import storage

sys.path.append("../")

from config import app_config
from domain.services.data_service import download_convs_hist
from domain.services.gcp_service import upload_df_to_bq


logger = getLogger()

ticker: str = "sh113011"
projectId = app_config.GCP_PROJECT
dataset = app_config.BQ_DATASET
table = f"{ticker}_hist"

# download convertable data
df = download_convs_hist(ticker=ticker, save_local=False)
if df.shape[0] > 0:
    # TODO check schema
    # TODO async
    result = upload_df_to_bq(df, projectId, dataset, table)


# storage api not supported yet
# def runWritePendingStream(projectId, dataset, table):
#     writePendingStream(projectId, dataset, table)


# def writePendingStream(projectId, dataset, table):
#     client = BigQueryWriteAsyncClient()
#     stream = WriteStream(WriteStream.Type.PENDING)
#     request = CreateWriteStreamRequest(
#         parent=f"projects/{projectId}/datasets/{dataset}/tables/{table}",
#         write_stream=stream,
#     )
#     write_stream = client.append_rows(request)
